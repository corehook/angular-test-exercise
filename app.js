var app = angular.module('loginApp', []).

factory('Auth', ['$http', function($http) {
    var Auth;
    Auth = (function() {
      var users = [];

      function Auth() {}

      Auth.prototype.users = function(){
        return users;
      };

      Auth.prototype.register = function(data) {
        console.log('Registaer user :' + data.email + ' with password :' + data.password);
        users.push(data);
      };

      return Auth;

    })();
    return new Auth();
  }
]).

controller('RegistrationController', ['$scope', 'Auth', function($scope, Auth) {
    $scope.users = Auth.users();
    $scope.register = function() {
      console.log('Register with data :'+$scope.registration_data)
      Auth.register($scope.registration_data);
    };

  }
]);
angular.element(document).ready(function() {
  angular.bootstrap(document, ['loginApp']);
});
